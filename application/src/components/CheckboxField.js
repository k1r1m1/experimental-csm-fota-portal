import React, { useState } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export default function CheckboxField({name, onUpdate}) {
  const [checked, setCheck] = useState();

  const handleChange = e => {
    setCheck(e.target.checked);
  }

  return (
    <FormControlLabel control={<Checkbox color='primary' checked={checked} onChange={handleChange} />} label={name} onBlur={() => onUpdate(checked)} />
  );
}