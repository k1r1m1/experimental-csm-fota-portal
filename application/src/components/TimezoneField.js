import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';

import Timezones from '../utils/data/Timezones';

const useStyles = makeStyles(theme => ({
  root: { margin:theme.spacing(1) },
  formControl: { minWidth:200, display:'flex' },
}));

export default function TimezoneField() {
  const [timezone, setTimezone] = useState({});
  const classes = useStyles()
  const items = Timezones ? Timezones.map(t => <MenuItem key={t.value} value={t}>{t.text}</MenuItem>) : <MenuItem>Loading...</MenuItem>;

  const handleChange = e => setTimezone(e.target.value);

  return (
    <form className={classes.root}>
      <FormControl variant='outlined' className={classes.formControl}>
        <Typography>Timezone</Typography>
        <Select value={timezone} onChange={handleChange}>{items}</Select>
      </FormControl>
    </form>
  );
}