import React, { Component } from 'react';
import { GoogleLogin } from 'react-google-login-component';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

import logoImage from '../images/logo-menu-bar.svg';
import CoresvcRequest from '../utils/coresvc-request';

const styles = {
  page: {
    backgroundColor: '#00254A',
    height: '100vh',
    overflow: 'hidden',
  },
  logo: {
    width: '100%',
    maxWidth: '300px',
  },
  button: {
    padding: 5,
    backgroundColor: '#ffffff',
    borderRadius: 5,
  },
  btnIcon: {
    height: '2em',
    marginRight: '0.7em',
  },
};

export default class LoginPage extends Component {
  state = {};

  handleGoogleLogin = async googleUser => {
    this.setState({ loginInProgress:true });

    // Get data from Google sign
    const profile = googleUser.getBasicProfile();
    const { id_token } = googleUser.getAuthResponse();
    const emailAddress = profile.getEmail();

    try {
      const coresvc = new CoresvcRequest({}, this.props.rootUrl);
      await coresvc.post('login', { id_token });
      this.props.onSuccess(profile);
    } catch(err) {
      const message =
          err.status === 403 ? `Your account does not have ${this.props.coresvcRole} privileges.  Please contact technical support.` :
          err.status === 404 ? `No account found for this email address ${emailAddress}` :
          'Unexpected response received from server: ' + err.status;
      alert(`Failed to log in: ${message}`);
      this.setState({ loginInProgress:false });
    }
  };

  render() {
    const { loginInProgress } = this.state;

    const buttonContent = (
      <Grid container spacing={0} alignItems="center">
        <Grid item xs={2}>
          <img alt="" style={styles.btnIcon}
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"/>
        </Grid>
        <Grid item xs={10}>
          Sign in With Google
        </Grid>
      </Grid>
    );

    return (
      <Grid container style={styles.page} alignItems="center">
        <Grid container direction="column" alignItems="center" spacing={10}>
          <Grid item xs>
            <img alt="PayGo Energy" style={styles.logo} src={logoImage}/>
          </Grid>
          <Grid item xs>
            {loginInProgress ?
              <CircularProgress color="inherit" style={{color:'white'}}/> :
              <GoogleLogin
                  style={styles.button}
                  socialId={this.props.googleClientId}
                  className="google-login"
                  scope="profile"
                  fetchBasicProfile
                  responseHandler={this.handleGoogleLogin}
                  buttonText={buttonContent}/>
            }
          </Grid>
        </Grid>
      </Grid>
    );
  }
}