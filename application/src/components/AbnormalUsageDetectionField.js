import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles(theme => ({
  root:{ margin:theme.spacing(1) },
  textField:{ width:200, marginBottom:theme.spacing(1) }
}));

export default ({name, checkbox, onUpdate}) => {
  const [value1, setValue1] = useState();
  const [value2, setValue2] = useState();
  const [checked, setChecked] = useState();
  const classes = useStyles();

  const handleValue1 = e => setValue1(e.target.value);
  const handleValue2 = e => setValue2(e.target.value);
  const handleChecked = e => setChecked(e.target.checked);

  useEffect(() => {
    onUpdate([value1,value2]);
  },[value1, value2, onUpdate]);

  return (
    <div className={classes.root}>
      <div>
        {checkbox && <FormControlLabel control={<Checkbox color='primary' checked={checked} onChange={handleChecked} />} label={name} />}
        <TextField
          className={classes.textField}
          type='number'
          defaultValue={0}
          inputProps={{ min:0 }}
          variant='outlined'
          value={value1}
          onChange={handleValue1}
          InputProps={{
            endAdornment:<InputAdornment position='end'>g/h</InputAdornment>
            }}
          disabled={!checked}
        /> <br/>
        <TextField
          className={classes.textField}
          type='number'
          defaultValue={0}
          inputProps={{ min:0 }}
          variant='outlined'
          value={value2}
          onChange={handleValue2}
          InputProps={{
            endAdornment:(<InputAdornment position='end'>min</InputAdornment>)
          }}
          disabled={!checked}
        />
      </div>
    </div>
  )
};