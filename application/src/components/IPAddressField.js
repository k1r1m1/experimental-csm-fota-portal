import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root:{ margin:theme.spacing(1) }
}));

export default function IPAddressField({onUpdate}) {
  const [ip, setIP] = useState();
  const classes = useStyles();

  const validateIPValue = () => {
    return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip);
  };

  const handleChange = e => {
    setIP(e.target.value);
  }

  return (
    <div className={classes.root}>
      <Typography>IP Address</Typography>
      <TextField label='IP Address' variant='outlined' value={ip} onChange={handleChange} error={!validateIPValue} onBlur={() => onUpdate(ip)} />
    </div>
  );
};