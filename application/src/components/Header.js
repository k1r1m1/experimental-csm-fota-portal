import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';

import logoImage from '../images/logo-menu-bar.svg';

const useStyles = makeStyles(theme => ({
  logo: { height:'2em', marginRight:'1em' },
}));

export default props => {
  const { title } = props;
  const classes = useStyles();

  return (
    <>
      <AppBar position='static'>
        <ToolBar>
          <IconButton edge='start' aria-label='menu' color='inherit'>
            <MenuIcon />
          </IconButton>
          <img alt="PayGo Energy" src={logoImage} className={classes.logo}/>
          <Typography variant='h6' color='inherit'>CSM {title}</Typography>
        </ToolBar>
      </AppBar>
    </>
  )
}