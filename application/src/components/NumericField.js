import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles(theme => ({
  root:{ margin:theme.spacing(1) },
  textField:props => props.checkbox ? ({ width:200 }) : ({ width:250 }),
}));

export default function NumericField({name, unit, checkbox, onUpdate}) {
  const [value, setValue] = useState();
  const [checked, setChecked] = useState();
  const classes = useStyles({checkbox});

  const handleValueChange = e => setValue(e.target.value);
  const handleChecked = e => setChecked(e.target.checked);

  return (
    <div className={classes.root}>
      {!checkbox && <Typography >{name}</Typography>}
      {checkbox && <FormControlLabel control={<Checkbox color='primary' checked={checked} onChange={handleChecked} />} label={name} />}
      <TextField
        className={classes.textField}
        type='number'
        defaultValue={0}
        inputProps={{ min:0 }}
        variant='outlined'
        value={value}
        onChange={handleValueChange}
        InputProps={{
          endAdornment:{unit} ? (<InputAdornment position='end'>{unit}</InputAdornment>) : null
          }}
        disabled={checkbox ? !checked : false}
        onBlur={() => onUpdate(value)}
      />
    </div>
  );
}