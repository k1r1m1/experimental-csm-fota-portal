import React, { useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root:{ margin:theme.spacing(1) },
  textField:{ width:240 },
  body:{ margin:theme.spacing(3) },
}));

export default function UploadTimeField({onUpdate}) {
  const [selectedTime1, setSelectedTime1] = useState();
  const [selectedTime2, setSelectedTime2] = useState();

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.body}>
        <Typography>Upload Time 1</Typography>
        <TextField className={classes.textField} type='time' defaultValue='00:00' value={selectedTime1}  InputLabelProps={{ shrink:true }} onChange={(e) => setSelectedTime1(e.currentTarget.value)} onBlur={() => onUpdate(selectedTime1)} />
      </div>
      <div className={classes.body}>
        <Typography>Upload Time 2</Typography>
        <TextField className={classes.textField} type='time' defaultValue='00:00' value={selectedTime2}  InputLabelProps={{ shrink:true }} onChange={(e) => setSelectedTime2(e.currentTarget.value)} onBlur={() => onUpdate(selectedTime2)} />
      </div>
    </div>
  )
}