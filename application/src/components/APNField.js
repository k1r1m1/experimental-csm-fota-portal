import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography  from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root:{ margin:theme.spacing(1) }
}));

export default function APNField({onUpdate})  {
  const [apn, setAPN ] = useState();
  const classes = useStyles();

  const handleChange = e => {
    setAPN(e.target.value);
  };


  return (
    <div className={classes.root}>
      <Typography>Access Point Name</Typography>
      <TextField label='APN' variant='outlined' value={apn} onChange={handleChange} onBlur={() => onUpdate(apn)} />
    </div>
  );
}