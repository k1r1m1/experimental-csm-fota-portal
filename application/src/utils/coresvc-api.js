import config from '../config';
import CoresvcRequest from './coresvc-request';

const coresvc = new CoresvcRequest({ globalResponseHandler }, config.REACT_APP_PAYGO_API_URL, 'csm-fota');

export const apiGet = coresvc.get;
export const apiPost = coresvc.post;

function globalResponseHandler(path, opts, res) {
  if (res.status === 403 && path !== '/login') {
    window.location = '/';
    return true;
  }
}
