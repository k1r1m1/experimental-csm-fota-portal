/**
 * Extension of fetch() to be used when talking to PayGo API (coresvc).
 *
 * This wrapper ensures that the `credentials` option is correctly sent, so that
 * calls to API include the session cookie.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
 *
 * N.B. this code is shared between web and react-native applications, so if
 * updating it in one place, please ensure it is also updated in the other.
 */
export default class CoresvcRequest {
  constructor(opts, ...rootPath) {
    this.rootPath = rootPath.join('/');

    this.globalResponseHandler = opts.globalResponseHandler;

    this.baseRequestOpts = {
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...opts.headers,
      },
    };
  }

  get = (path, queryParams) => {
    const pathWithParams = path + (!queryParams ? '' :
        `?${
            Object.keys(queryParams)
                .map(k => `${k}=${encodeURIComponent(queryParams[k])}`)
                .join('&')
        }`);

    return this.apiFetch(pathWithParams);
  }

  post = (path, body) => this.apiFetch(path, { method:'POST', body:JSON.stringify(body) });

  async apiFetch(path, opts={}) {
    const res = await fetch(`${this.rootPath}/${path}`, { ...this.baseRequestOpts, ...opts });

    if(this.globalResponseHandler) {
      const handled = await this.globalResponseHandler(path, opts, res);
      if(handled) return;
    }

    const bodyText = await res.text();

    try {
      const body = JSON.parse(bodyText);
      if(res.status >= 300) {
        const message = body.error || JSON.stringify(body);
        throw responseError(new Error(message), res, body);
      }
      return body;
    } catch(err) {
      throw responseError(err, res, bodyText);
    }
  }
}

function responseError(err, { status }, body) {
  err.status = status;
  err.body = body;
  return err;
}
