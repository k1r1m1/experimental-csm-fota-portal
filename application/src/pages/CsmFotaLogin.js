import React from 'react';

import LoginPage from '../components/Login';
import config from '../config';


export default () => <LoginPage onSuccess={handleLogin}
  coresvcRole="csm-fota-manager"
  googleClientId={config.REACT_APP_GOOGLE_CLIENT_ID}
  rootUrl={`${config.REACT_APP_PAYGO_API_URL}/csm-fota`} />

const handleLogin = profile => {
  window.location = '/meters';
}