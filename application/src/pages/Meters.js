import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import Header from '../components/Header';
import APNField from '../components/APNField';
import IPAddressField from '../components/IPAddressField';
import NumericField from '../components/NumericField';
import TimezoneField from '../components/TimezoneField';
import UploadtimeField from '../components/UploadTimeField';
import CheckboxField from '../components/CheckboxField';
import AbnormalUsageDetectionField from '../components/AbnormalUsageDetectionField';
import LeakDetectionField from '../components/LeakDetectionField';

import { apiGet } from '../utils/coresvc-api';

const styles = {
  content:{ width:300 },
};

class Meters extends React.Component {
  state = {};

  componentDidMount() {
    apiGet('meters')
      .then(res => this.setState({ meters:res }))
      .catch(err => this.setState({ meters:{ error:err.message }}))
  }

  handleSelectedMeters = e => this.setState({ selectedMeters:e.currentTarget.value});

  handleAPNUpdate = apn => this.setState({ apn });

  handleIPUpdate = ip => this.setState({ ip });

  handlePortUpdate = port => this.setState({ port });

  handleSessionTimerUpdate = sessionTimer => this.setState({ sessionTimer });

  handleBalanceThresholdUpdate = balanceThreshold => this.setState({ balanceThreshold });

  handleBatteryThresholdUpdate = batteryThreshold => this.setState({ batteryThreshold });

  handleUploadTimeUpdate = uploadTime => this.setState({ uploadTime });

  handleInitialValveClose = initialValveClose => this.setState({ initialValveClose });

  handleFinalValveClose = finalValveClose => this.setState({ finalValveClose });

  handleCommLimitUpdate = commLimit => this.setState({ commLimit });

  handleLockTamperDetectionUpdate = lockTamperDetector => this.setState({ lockTamperDetector });

  handleEnclosureTamperDetectionUpdate = enclosureTamperDetector => this.setState({ enclosureTamperDetector });

  handleAutoLockUpdate = autoLock => this.setState({ autoLock });

  handleImpactDetectionUpdate = impactDetector => this.setState({ impactDetector });

  handleAbnormalUsageUpdate = alertUsageLimit => this.setState({ alertUsageLimit });

  handleLeakDetectionUpdate = leakDetector => this.setState({ leakDetector });

  handleSubmit = e => {};


  render() {
    const { classes } = this.props;

    return (
      <>
        <Header title='Meters' />
        <Paper>
          <Card>
            <Grid container justify='center'>
              <CardContent className={classes.content}>
                <APNField onUpdate={this.handleAPNUpdate} />
                <IPAddressField onUpdate={this.handleIPUpdate}/>
                <NumericField name='Port' onUpdate={this.handlePortUpdate} />
              </CardContent>
              <CardContent className={classes.content}>
                <NumericField name='Session Timer Limit' unit='sec' onUpdate={this.handleSessionTimerUpdate} />
                <NumericField name='Low Balance Threshold' unit='grams' onUpdate={this.handleBalanceThresholdUpdate} />
                <NumericField name='Low Battery Threshold' unit='volts' onUpdate={this.handleBatteryThresholdUpdate} />
              </CardContent>
              <CardContent className={classes.content}>
                <UploadtimeField onUpdate={this.handleUploadTimeUpdate} />
                <TimezoneField />
              </CardContent>
              <CardContent className={classes.content}>
                <NumericField name='Initial Valve Close' unit='grams' onUpdate={this.handleInitialValveClose} />
                <NumericField name='Final Valve Close' unit='grams' onUpdate={this.handleFinalValveClose} />
                <NumericField name='No Comm Limit' unit='hours' onUpdate={this.handleCommLimitUpdate} />
                <CheckboxField name='Lock Tamper Detection' onUpdate={this.handleLockTamperDetectionUpdate} /> <br />
                <CheckboxField name='Enclosure Tamper Detection' onUpdate={this.handleEnclosureTamperDetectionUpdate} /> <br />
                <CheckboxField name='Auto Lock' onUpdate={this.handleAutoLockUpdate} />
              </CardContent>
              <CardContent className={classes.content}>
                <NumericField name='Impact Detection' unit='g*force' checkbox onUpdate={this.handleImpactDetectionUpdate} />
                <AbnormalUsageDetectionField name='Abnormal Usage Detection' checkbox onUpdate={this.handleAbnormalUsageUpdate} />
                <LeakDetectionField name='Leak Detection' checkbox onUpdate={this.handleLeakDetectionUpdate}/>
              </CardContent>
            </Grid>
          </Card>
          <Card>
            <CardContent>
              <FormControl fullWidth variant='outlined'>
                <InputLabel>Choose Meters</InputLabel>
                <OutlinedInput value={this.state.selectedMeters} onChange={this.handleSelectedMeters} labelWidth={110} />
              </FormControl>
            </CardContent>
            <CardActions>
              <Grid container justify='center'>
                <Button variant='contained' color='primary' onClick={this.handleSubmit}>Submit</Button>
              </Grid>
            </CardActions>
          </Card>
        </Paper>
      </>
    );
  }
}

export default withStyles(styles)(Meters);