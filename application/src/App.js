import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import CsmFotaLogin from './pages/CsmFotaLogin';
import Meters from './pages/Meters';

const App = () => {
  return (
    <Switch>
      <Route exact path='/login' component={CsmFotaLogin} />
      <Route path='/meters' component={Meters} />
      <Redirect from='/' to='/login' />
    </Switch>
  )
}

export default App;